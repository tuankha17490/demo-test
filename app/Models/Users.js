import Model from './Schema'
import Shares from './Share'
export default class Users extends Model {
    static get tableName() {
        return 'users'
    }
    static get idColumn() {
        return 'id'
    }
    // Modifiers are reusable query snippets that can be used in various places.
    static get Modifier() {
        return {
            searchByName(query, name) {
                // ......
            }
        }
    }
    // To do validate 
    static get jsonSchema() {
        return {
            type: 'object',
            required: ['email', 'password'],
            properties: {
                id: {
                    type: 'integer'
                },
                password: {
                    type: "string",
                    minLength: 6
                },
                email: {
                    type: "string",
                    format: "email"
                },
            },
        }
    }
    static get relationMappings() {
        return {
            shares: {
                relation: Model.HasManyRelation,
                modelClass: Shares,
                join: {
                    from: 'users.id',
                    to: 'shares.user_id'
                }
            }
        }
    }
}