import request from 'supertest';
import jwt from "jsonwebtoken"
import app from '../../app';
import Model from '../../Models/Schema'
describe('Share Endpoint', () => {
    const token = jwt.sign({
        id: 1,
    }, process.env.JWT_KEY, {
        expiresIn: "2h"
    })
    afterAll(async () => {
        await Model.knex().destroy();
    });
    it('should create a new share', async () => {
        await request(app)
        .post('/api/v1/shares')
        .send({
            content: 'https://www.youtube.com/watch?v=FmSl7nfW2Ys',
        })
        .set('Accept', 'application/json')
        .set('authorization', token)
        .expect(201)
        .then(async (res) => {
            expect(res.body.data).toHaveProperty('content');
        })
    })

    it('should get list shares that was created', async () => {
        await request(app)
        .get('/api/v1/shares')
        .expect(200)
        .then(async (res) => {
            expect(res.body).not.toHaveLength(0)
        })
    })
})