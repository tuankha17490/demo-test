import express from "express"
const router = express.Router();
import ShareController from "./controller"
import authorization from "../../../Middleware/check_authorize"
const controller = new ShareController()
import {ReasonPhrases, StatusCodes} from 'http-status-codes'
import ShareValidator from "./validator"
const validator = new ShareValidator()


router.get('/',(req, res) => {
    try {
        controller.getList().then(result => {return res.status(StatusCodes.OK).json(result)})
    } catch (error) {
        console.log('CONTROLLER_GET_SHARE_LIST');
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({error: ReasonPhrases.INTERNAL_SERVER_ERROR})
    }
})
router.post('/', authorization, validator.create, (req, res) => {
    try {
        controller.create(req).then(result => {return res.status(result.status).json(result)})
    } catch (error) {
        console.log('CONTROLLER_CREATE_NEW_SHARE');
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({error: ReasonPhrases.INTERNAL_SERVER_ERROR})
    }
})

export default router;
