import UserRespository from "./respository"
import BaseServices from '../../core/Service';
import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import dotenv from "dotenv"
import process from "process"
import {ReasonPhrases, StatusCodes} from 'http-status-codes'
dotenv.config({ silent: process.env.NODE_ENV === 'production' });
export default class UserService extends BaseServices {
    static _Instance;
    static Instance() {
        if(!this._Instance) {
            this._Instance = new this();
        }
        return this._Instance;
    }
    getModule() {
        return UserRespository.Instance();
    }

    async registerAndLogin(param) {
        try {
            const checkUser = await this.respository.getBy({email: param.email})
            let token;
            if(checkUser) {
                const checkPassWordHashed = bcrypt.compareSync(param.password, checkUser.password)
                if(checkPassWordHashed) {
                    token = jwt.sign({
                        email: checkUser.email,
                        id: checkUser.id,
                    }, process.env.JWT_KEY, {
                        expiresIn: "2h"
                    })
                }
                else {
                    return {
                        status: StatusCodes.UNAUTHORIZED,
                        message: ReasonPhrases.UNAUTHORIZED
                    }
                }
            }
            else {
                param.password = bcrypt.hashSync(param.password,10)
                const dataFetch = await this.respository.create(param);
                token = jwt.sign({
                    email: dataFetch.email,
                    id: dataFetch.id,
                }, process.env.JWT_KEY, {
                    expiresIn: "2h"
                })
            }
            return {
                status: StatusCodes.OK,
                message: ReasonPhrases.OK,
                data: {token}
            };
        } catch (error) {
            console.log(error);
            throw error;
        }
    }
}