import UserRoute from "../modules/auth/routes"
import ShareRoute from "../modules/share/routes.js"
import express from "express"
const app = express();

app.use('/auth',UserRoute);
app.use('/shares',ShareRoute);

export default app;