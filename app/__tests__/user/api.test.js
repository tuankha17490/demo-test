import request from 'supertest';
import app from '../../app';
import Model from '../../Models/Schema'
import UserModel from '../../Models/Users'
import bcrypt from 'bcrypt';
import { StatusCodes } from 'http-status-codes';
describe('Authentication Endpoint', () => {
    afterAll(async () => {
        await Model.knex().destroy();
    });
    it('should register or login', async () => {
        const password = bcrypt.hashSync('123456', 10)
        await UserModel.query().insert({email: "tuankha@gmail.com", password})
        const res = await request(app).post('/api/v1/auth').send({email: "tuankha@gmail.com", password: '123456'})
        expect(res.statusCode).toBe(StatusCodes.OK);
    })
})