import { StatusCodes } from 'http-status-codes';
import request from 'supertest';
import app from '../../app';
import UserModel from '../../Models/Users'
import bcrypt from 'bcrypt';
import Model from '../../Models/Schema'
describe('Validate payload request', () => {
    afterAll(() => {Model.knex().destroy()})
    it('Should send the email when login or register', async () => {
        const res = await request(app).post('/api/v1/auth').send({password: 'asd'})
        expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
    })
    it('Should send the password when login or register', async () => {
        const res = await request(app).post('/api/v1/auth').send({email: 'asd@gmail.com'})
        expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
    })
    it('Should send the right the password to login', async () => {
        const password = bcrypt.hashSync('123456', 10)
        await UserModel.query().insert({email: "tuankha@gmail.com", password})
        const res = await request(app).post('/api/v1/shares').send({email: "tuankha@gmail.com", password: '1234'})
        expect(res.statusCode).toBe(StatusCodes.UNAUTHORIZED);
    })
})