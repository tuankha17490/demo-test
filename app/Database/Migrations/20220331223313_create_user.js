
exports.up = function(knex) {
    return knex.schema.createTable('users', t => {
        t.increments('id')
        t.string('email').notNull();
        t.string('password').notNull();
        t.timestamp('updated_At').defaultTo(knex.fn.now());
        t.timestamp('created_At').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('users')
};
