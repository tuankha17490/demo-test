import ShareRespository from "./respository"
import BaseServices from '../../core/Service';
import dotenv from "dotenv"
import process from "process"
import { ReasonPhrases, StatusCodes } from "http-status-codes";
dotenv.config({ silent: process.env.NODE_ENV === 'production' });
export default class ShareService extends BaseServices {
    static _Instance;
    static Instance() {
        if(!this._Instance) {
            this._Instance = new this();
        }
        return this._Instance;
    }
    getModule() {
        return ShareRespository.Instance();
    }

    async create(req) {
        const {body, userData} = req;
        try {
            const {content} = body;
            const user_id = userData.id;
            const result = await this.respository.create({user_id, content});
            return {
                status: StatusCodes.CREATED,
                message: ReasonPhrases.CREATED,
                data: result
            };
        } catch (error) {
            console.log(error);
            throw error;
        }
    }
}