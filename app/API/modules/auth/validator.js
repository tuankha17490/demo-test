import BaseValidator from "../../core/Validator"
export default class UserValidator extends BaseValidator {
    constructor() {
        super()
    }
    registerTask(req, res, next) {
        try { 
            const checkEmail = super.emailValidate(req, res)
            const checkPassword = super.passwordValidate(req, res)
            if(checkEmail) return checkEmail;
            else if(checkPassword) return checkPassword;
            else next()
        } catch (error) {
            return res.status(400).json({
                status: 400,
                error: error.toString()
            })
        }
    }
}