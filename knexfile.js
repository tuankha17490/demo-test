require('dotenv').config();

module.exports = {
  development: {
      client:  process.env.CLIENT,
      connection: {
        host: process.env.HOST,
        user: process.env.User,
        password: process.env.PASSWORD,
        database: process.env.DATABASE
      },
      pool: { min: 0, max: 7 },
      debug: true,
      migrations: {
        directory: './app/Database/Migrations'
      },
      seed: {
        directory: './app/Database/Seeds'
      }
  }
};

