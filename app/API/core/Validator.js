import validator from "validator"
export default class BaseValidator {
    emailValidate(req, res) {
        try {
            if (!req.body.email || !validator.isEmail(req.body.email)) {
                return res.status(400).json({
                    status: 400,
                    error: 'Email is invalid'
                })
            }
            return false
        } catch (error) {
            return res.status(400).json({
                status: 400,
                message: 'Email error',
                error: error.toString()
            })
        }
    }
    passwordValidate(req, res) {
        try {
            if (!req.body.password || !validator.isAlphanumeric(req.body.password)) {
                return res.status(400).json({
                    status: 400,
                    error: 'Paswword is invalid',
                    message: 'Password is only number,alphabet'
                })
            }
            return false
        } catch (error) {
            return res.status(400).json({
                status: 400,
                message: 'Password error',
                error: error.toString()
            })
        }
    }
}