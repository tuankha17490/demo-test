import BaseRespository from '../../core/Repository'
import Shares from "../../../Models/Share"
export default class ShareRespository extends BaseRespository {
    static _Instance;
    static Instance() {
        if (!this._Instance) {
            this._Instance = new this();
        }
        return this._Instance;
    }
    getTable() {
        return Shares;
    }
}