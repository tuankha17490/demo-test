
exports.up = function(knex) {
    return knex.schema.createTable('shares', t => {
        t.increments('id')
        t.string('content').notNull();
        t.timestamp('updated_at').defaultTo(knex.fn.now());
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.integer('user_id').unsigned();
        t.foreign('user_id').references('users.id');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('shares')
};
