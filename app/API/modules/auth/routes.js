import express from "express"
const router = express.Router();
import UserController from "./controller"
import UserValidator from "./validator"
import {ReasonPhrases, StatusCodes} from 'http-status-codes'
const controller = new UserController()
const validator = new UserValidator()

router.post('/', validator.registerTask, (req, res) => {
    try {
        controller.registerAndLogin(req.body).then(result => {return res.status(result.status).json(result)})
    } catch (error) {
        console.log('CONTROLLER_LOGIN_OR_REGISTER');
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({error: ReasonPhrases.INTERNAL_SERVER_ERROR})
    }
})

export default router;
