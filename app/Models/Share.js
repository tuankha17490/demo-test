import Model from './Schema'
import Users from './Users'
export default class Shares extends Model {
    static get tableName() {
        return 'shares'
    }
    static get idColumn() {
        return 'id'
    }
    // To do validate 
    static get jsonSchema() {
        return {
            type: 'object',
            required: ['content'],
            properties: {
                id: {
                    type: 'integer'
                },
                content: "string",
            },
        }
    }
    static get relationMappings() {
        return {
            users: {
                relation: Model.HasOneRelation,
                modelClass: Users,
                join: {
                    from: 'shares.user_id',
                    to: 'users.id'
                }
            }
        }
    }
}