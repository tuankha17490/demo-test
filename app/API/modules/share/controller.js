import BaseController from '../../core/Controller'
import ShareService from "./service"
export default class ShareController extends BaseController {
    constructor() {
        super();
    }
    getModule() {
        return ShareService.Instance();  
    }
}