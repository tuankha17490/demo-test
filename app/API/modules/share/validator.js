import BaseValidator from "../../core/Validator"
import validator from "validator"
import { ReasonPhrases, StatusCodes } from "http-status-codes";
export default class ShareValidator extends BaseValidator {
    constructor() {
        super()
    }
    create(req, res, next) {
        try {
            if(!req.body.content || !validator.isURL(req.body.content) || !req.body.content.includes('youtube')) return res.status(StatusCodes.BAD_REQUEST).json({error: true, message: ReasonPhrases.BAD_REQUEST})
            else next()
        } catch (error) {
            return res.status(400).json({
                status: 400,
                error: error.toString()
            })
        }
    }
}