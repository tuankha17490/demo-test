module.exports = {
    testEnvironment: "node",
    coveragePathIgnorePatterns: [
        "/node_modules/"
    ],
    testMatch: ['<rootDir>/app/__tests__/**'],
};