import { StatusCodes } from 'http-status-codes';
import jwt from "jsonwebtoken"
import request from 'supertest';
import app from '../../app';
describe('Validate payload request', () => {
    afterAll((done) => {done()})
    const token = jwt.sign({
        id: 1,
    }, process.env.JWT_KEY, {
        expiresIn: "2h"
    })
    it('Should login before create share', async () => {
        const res = await request(app).post('/api/v1/shares').send({})
        expect(res.statusCode).toBe(StatusCodes.UNAUTHORIZED);
    })
    it('Should send the required payload when create', async () => {
        const res = await request(app).post('/api/v1/shares').send({}).set('Accept', 'application/json').set('authorization', token)
        expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
    })
    it('Should send the payload that is url', async () => {
        const res = await request(app).post('/api/v1/shares').send({content: 'asdasd'}).set('Accept', 'application/json').set('authorization', token)
        expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
    })
})